## About
This program will search current UICN status of multiple species from their wikipedia pages.

UICN does not distribute its list (you have to ask them).

"Pipistrellus pygmaeus" will be redirected to "Pipistrelle soprane" on fr wikipedia.

## Dependencies
You will need requests to make this program work:
```
python3 -m pip install requests
```

## Usage
```
python3 getUICNstatus.py file.txt
```
Where file.txt have a name per line.

## Example ?
Just try with the file named example.txt.
```
python3 getUICNstatus.py example.txt
```


## Screenshot ?

Sure.

![output](https://i.imgur.com/eCPxBb5.png)
