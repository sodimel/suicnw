#!/usr/bin/python3
# coding: utf-8

"""
	parse.py
	MediaWiki Action API Code Samples
	src:		https://github.com/wikimedia/MediaWiki-Action-API-Code-Samples/blob/master/python/parse.py
	wiki src:	https://www.mediawiki.org/wiki/API:Parsing_wikitext#Sample_code
	author:		Srishti Sethi - https://github.com/srish
	MIT license

	getUICNstatus.py
	Search species UICN status from Wikipedia
	Add by Corentin Bettiol <corentin@244466666.xyz>:
	- load file
	- extract wikipedia title pages
	- search UICN status on content of wikipedia pages
	- display it
	src : 		https://gitlab.com/sodimel/suicnw
	WTFPLv2 License
"""

#wtfpl
import sys
import re
import requests

if len(sys.argv) != 2 and len(sys.argv) != 3:
	print("Invalid nb of args !")
	print("")
	print("Usage:\tpython3 getUICNstatus.py file.txt")
	print("      \t  Where file.txt contain one name per line.")
	print("      \tor")
	print("      \tpython3 getUICNstatus.py file.txt lang")
	print("      \t  Where lang is the wikipedia language code.")
	print("      \t  (ex: fr, en, it, es...)")
	print("Wikipedia redirects are supported")
	print("\t(ex: \"Pipistrellus pygmaeus\" is redirected to \"Pipistrelle soprane\" in fr wikipedia.")
	exit()

#wtfpl
lang = sys.argv[2] if len(sys.argv) == 3 else "en"

#mit
S = requests.Session()
URL = "https://" + lang + ".wikipedia.org/w/api.php"

#wtfpl
TEXT_FILE = open(sys.argv[1], "r")
LINES = TEXT_FILE.read().split('\n')
for title in LINES:
	#mit
	PARAMS = {
		'action': "parse",
		'page': title,
		'format': "json",
		'redirects': "true"
	}
	R = S.get(url=URL, params=PARAMS)
	
	#wtfpl
	data = R.text
	status = re.search("( [A-Z]{2} )", data)

	if status is not None:
		status = status.group().strip()

	if status is not None and len(status) == 2:
		print(str(title) + " : " + str(status))
	else:
		print(str(title) + " : Not found")
